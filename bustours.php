<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>EmpireOne Travel - Travel Agency in Toronto | Cheap Flights to Manila</title>
  <link rel="shortcut icon" href="images/logo.png"/>

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" />
  <!-- <link href="css/button-style.css" rel="stylesheet" /> -->

  <!-- <link rel="stylesheet" href="css/styles.css"> -->

  <!-- =======================================================
    Theme Name: Company
    Theme URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->

  <?php include('header.php'); ?>

  <style media="screen">
/* ------------------------------------------------- */

.sud {
  width: 80%;
  display: flex;
  align-items: center;
  justify-content: space-between;
}

button {
  position: relative;
  margin: 0;
  padding: 5px 12px;
  height: 60px;
  width: 150px;
  outline: none;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  text-transform: uppercase;
  background-color: #ffffff;
  border: 1px solid rgba(22, 76, 167, 0.6);
  border-radius: 10px;
  color: #1d89ff;
  font-weight: 400;
  font-size: 20px;
  font-family: inherit;
  z-index: 0;
  overflow: hidden;
  transition: all 0.3s cubic-bezier(0.02, 0.01, 0.47, 1);
}
button span {
  color: #164ca7;
  font-size: 12px;
  font-weight: 500;
  letter-spacing: 0.7px;
}
button:hover {
  animation: rotate 0.7s ease-in-out both;
}
button:hover span {
  animation: storm 0.7s ease-in-out both;
  animation-delay: 0.06s;
}
@keyframes rotate {
  0% {
    transform: rotate(0deg) translate3d(0, 0, 0);
  }
  25% {
    transform: rotate(3deg) translate3d(0, 0, 0);
  }
  50% {
    transform: rotate(-3deg) translate3d(0, 0, 0);
  }
  75% {
    transform: rotate(1deg) translate3d(0, 0, 0);
  }
  100% {
    transform: rotate(0deg) translate3d(0, 0, 0);
  }
}
@keyframes storm {
  0% {
    transform: translate3d(0, 0, 0) translateZ(0);
  }
  25% {
    transform: translate3d(4px, 0, 0) translateZ(0);
  }
  50% {
    transform: translate3d(-3px, 0, 0) translateZ(0);
  }
  75% {
    transform: translate3d(2px, 0, 0) translateZ(0);
  }
  100% {
    transform: translate3d(0, 0, 0) translateZ(0);
  }
}
.btn-pill:before, .btn-pill:after {
  position: absolute;
  right: 0;
  bottom: 0;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: #1d89ff;
  content: '';
  opacity: 0;
  transition: transform 0.15s cubic-bezier(0.02, 0.01, 0.47, 1), opacity 0.15s cubic-bezier(0.02, 0.01, 0.47, 1);
  z-index: -1;
  transform: translate(100%, -25%) translate3d(0, 0, 0);
}
.btn-pill:hover:before, .btn-pill:hover:after {
  opacity: 0.15;
  transition: transform 0.2s cubic-bezier(0.02, 0.01, 0.47, 1), opacity 0.2s cubic-bezier(0.02, 0.01, 0.47, 1);
}
.btn-pill:hover:before {
  transform: translate3d(50%, 0, 0) scale(0.9);
}
.btn-pill:hover:after {
  transform: translate(50%, 0) scale(1.1);
}
/* ------------------------------------------------- */

  /* Gallery */

  	@-moz-keyframes gallery {
  		100% {
  			opacity: 1;		}
  	}

  	@-webkit-keyframes gallery {
  		100% {
  			opacity: 1;		}
  	}

  	@-ms-keyframes gallery {
  		100% {
  			opacity: 1;		}
  	}

  	@keyframes gallery {
  		100% {
  			opacity: 1;		}
  	}

  	.gallery {
  		padding: 3.5em;
  		position: relative;
  		overflow: hidden;
  		min-height: 37em;
  	}

  		@media screen and (max-width: 980px) {

  			.gallery {
  				padding: 2em;
  				min-height: 20em;
  			}

  				.gallery header h2 {
  					margin-bottom: 1em;
  				}

  		}

  		@media screen and (max-width: 480px) {

  			.gallery {
  				padding: 1em;
  			}

  		}

  		.gallery header {
  			display: -ms-flexbox;
  			-ms-flex-pack: justify;
  			display: -moz-flex;
  			display: -webkit-flex;
  			display: -ms-flex;
  			display: flex;
  			-moz-flex-wrap: wrap;
  			-webkit-flex-wrap: wrap;
  			-ms-flex-wrap: wrap;
  			flex-wrap: wrap;
  			-moz-justify-content: space-between;
  			-webkit-justify-content: space-between;
  			-ms-justify-content: space-between;
  			justify-content: space-between;
  		}

  			.gallery header.special {
  				-moz-justify-content: center;
  				-webkit-justify-content: center;
  				-ms-justify-content: center;
  				justify-content: center;
  			}

  			@media screen and (max-width: 736px) {

  				.gallery header {
  					display: block;
  				}

  			}

  			@media screen and (max-width: 480px) {

  				.gallery header h2 {
  					margin-bottom: .5em;
  				}

  			}

  		.gallery footer {
  			text-align: center;
  			margin-top: 4em;
  		}

  		.gallery .content {
  			display: -ms-flexbox;
  			display: -moz-flex;
  			display: -webkit-flex;
  			display: -ms-flex;
  			display: flex;
  			-moz-flex-wrap: wrap;
  			-webkit-flex-wrap: wrap;
  			-ms-flex-wrap: wrap;
  			flex-wrap: wrap;
  			-moz-justify-content: -moz-flex-start;
  			-webkit-justify-content: -webkit-flex-start;
  			-ms-justify-content: -ms-flex-start;
  			justify-content: flex-start;
  		}

  			.gallery .content .media {
  				-moz-animation: gallery 0.75s ease-out 0.4s forwards;
  				-webkit-animation: gallery 0.75s ease-out 0.4s forwards;
  				-ms-animation: gallery 0.75s ease-out 0.4s forwards;
  				animation: gallery 0.75s ease-out 0.4s forwards;
  				margin-bottom: 0;
  				overflow: hidden;
  				opacity: 0;
  				position: relative;
  				width: 25%;
  			}

  				.gallery .content .media a {
  					display: block;
  				}

  				.gallery .content .media img {
  					-moz-transition: -moz-transform 0.2s ease-in-out;
  					-webkit-transition: -webkit-transform 0.2s ease-in-out;
  					-ms-transition: -ms-transform 0.2s ease-in-out;
  					transition: transform 0.2s ease-in-out;
  					max-width: 100%;
  					height: auto;
  					vertical-align: middle;
  				}

  				.gallery .content .media:hover img {
  					-moz-transform: scale(1.075);
  					-webkit-transform: scale(1.075);
  					-ms-transform: scale(1.075);
  					transform: scale(1.075);
  				}

  				@media screen and (max-width: 736px) {

  					.gallery .content .media {
  						width: 50%;
  					}

  				}

  				@media screen and (max-width: 480px) {

  					.gallery .content .media {
  						width: 100%;
  					}

  				}


          /* Banner */

          	body.is-loading #banner > .inner {
          		opacity: 0;
          		-moz-transform: translateY(1em);
          		-webkit-transform: translateY(1em);
          		-ms-transform: translateY(1em);
          		transform: translateY(1em);
          	}

          	#banner {
          		display: -ms-flexbox;
          		-ms-flex-pack: center;
          		-ms-flex-align: center;
          		padding: 8em 0 6em 0;
          		-moz-align-items: center;
          		-webkit-align-items: center;
          		-ms-align-items: center;
          		align-items: center;
          		display: -moz-flex;
          		display: -webkit-flex;
          		display: -ms-flex;
          		display: flex;
          		-moz-justify-content: center;
          		-webkit-justify-content: center;
          		-ms-justify-content: center;
          		justify-content: center;
          		min-height: 75vh;
          		height: 75vh;
          		position: relative;
          		background: #000;
          		background-image: url(images/bustours/bss.jpg);
          		background-size: cover;
          		background-attachment: fixed;
          		background-repeat: no-repeat;
          		background-position: center;
          		text-align: center;
          		color: #FFF;
          	}

          		#banner:before {
          			-moz-transition: opacity 3s ease;
          			-webkit-transition: opacity 3s ease;
          			-ms-transition: opacity 3s ease;
          			transition: opacity 3s ease;
          			-moz-transition-delay: 0.25s;
          			-webkit-transition-delay: 0.25s;
          			-ms-transition-delay: 0.25s;
          			transition-delay: 0.25s;
          			content: '';
          			display: block;
          			background-color: #000;
          			height: 100%;
          			left: 0;
          			opacity: 0.65;
          			position: absolute;
          			top: 0;
          			width: 100%;
          			z-index: 1;
          		}

          		#banner .inner {
          			-moz-transform: none;
          			-webkit-transform: none;
          			-ms-transform: none;
          			transform: none;
          			-moz-transition: opacity 1s ease, transform 1s ease;
          			-webkit-transition: opacity 1s ease, transform 1s ease;
          			-ms-transition: opacity 1s ease, transform 1s ease;
          			transition: opacity 1s ease, transform 1s ease;
          			position: relative;
          			opacity: 1;
          			z-index: 3;
          			padding: 0 2em;
          		}

          		#banner h1 {
          			font-size: 4em;
          			line-height: 1em;
          			margin: 0 0 0.5em 0;
          			padding: 0;
          			color: #FFF;
          		}

          		#banner p {
          			font-size: 1.5em;
          			margin-bottom: 1.75em;
          		}

          		#banner a {
          			color: #FFF;
          			text-decoration: none;
          		}

          		@media screen and (max-width: 1280px) {

          			#banner h1 {
          				font-size: 3.5em;
          			}

          		}

          		@media screen and (max-width: 736px) {

          			#banner {
          				background-attachment: scroll;
          			}

          				#banner h1 {
          					font-size: 2.25em;
          				}

          				#banner p {
          					font-size: 1.25em;
          				}

          		}
              /* Section/Article */

              	section.special, article.special {
              		text-align: center;
              	}

              	section .inner, article .inner {
              		padding: 3.5em 0 1.5em 0;
              		padding-left: 3.5em;
              		padding-right: 3.5em;
              	}

              		@media screen and (max-width: 980px) {

              			section .inner, article .inner {
              				padding: 2em 0 0.1em 0;
              				padding-right: 2em;
              				padding-left: 2em;
              			}

              		}

              		@media screen and (max-width: 480px) {

              			section .inner, article .inner {
              				padding: 1em 0 0.1em 0;
              				padding-right: 1em;
              				padding-left: 1em;
              			}

              		}

              	section .columns, article .columns {
              		display: -ms-flexbox;
              		display: -moz-flex;
              		display: -webkit-flex;
              		display: -ms-flex;
              		display: flex;
              		-moz-flex-wrap: wrap;
              		-webkit-flex-wrap: wrap;
              		-ms-flex-wrap: wrap;
              		flex-wrap: wrap;
              		-moz-justify-content: space-between;
              		-webkit-justify-content: space-between;
              		-ms-justify-content: space-between;
              		justify-content: space-between;
              	}

              		section .columns.double .column, article .columns.double .column {
              			width: 48%;
              		}

              			@media screen and (max-width: 980px) {

              				section .columns.double .column, article .columns.double .column {
              					width: 100%;
              				}

              			}

              	header p {
              		position: relative;
              		margin: 0 0 1.5em 0;
              	}

              	header h2 + p {
              		font-size: 1.25em;
              		margin-top: -1em;
              	}

              	header h3 + p {
              		font-size: 1.1em;
              		margin-top: -0.8em;
              	}

              	header h4 + p,
              	header h5 + p,
              	header h6 + p {
              		font-size: 0.9em;
              		margin-top: -0.6em;
              	}

        .media-text {

          position: absolute;
          bottom: 0px;
          width: 100%;
          color: #5071A9;
          background: rgba(255,255,255,0.6);
        }

        .media-title {
          position: absolute;
          top: 0px;
          text-align: center;
          padding: 5px 0;
          background: rgba(255,255,255,0.3);
          width: 100%;
        }

        div:has(.btn-book) {
          padding-left: 300px;

        }
  </style>
  </header>

  <div id="breadcrumb">
    <div class="container">
      <div class="breadcrumb">
        <li><a href="index.html">Home</a></li>
        <li>Bus Tours</li>
      </div>
    </div>
  </div>

  <!-- Banner -->
    <section id="banner">
      <div class="inner">
        <h1>BUS TOURS</h1>
        <p style="color: white;">Empireone <a href="" style="color: green;">TRAVEL</a></p>
        <ul class="actions">
          <!-- <a href="#galleries" class="button alt scrolly big btn btn-success" style="opacity: 0.6;">Continue</a> -->
          <div class="container sud" >
            <a href="#galleries"class="alt scrolly " style="opacity: 0.6;">
            <button class="btn-pill">
              <span>Get More Photos</span>
            </button>
            </a>
          </div>
        </ul>
      </div>
    </section>

  <section id="galleries">

    <!-- Photo Galleries -->
      <div class="gallery">
        <header class="special">
          <h2>Latest News</h2>
        </header>
        <div class="content">
          <div class="media" style="margin-top:15px;">
            <a href="images1/fulls/05.jpg"><img src="images1/thumbs/05.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsumm
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/01.jpg"><img src="images1/thumbs/01.jpg" alt="" title="This left here is a caption. <br><div><button class='btn-book'>Book Now!</button></div>" /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/09.jpg"><img src="images1/thumbs/09.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/02.jpg"><img src="images1/thumbs/02.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/06.jpg"><img src="images1/thumbs/06.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/10.jpg"><img src="images1/thumbs/10.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/03.jpg"><img src="images1/thumbs/03.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
          <div class="media">
            <a href="images1/fulls/07.jpg"><img src="images1/thumbs/07.jpg" alt="" title="This right here is a caption." /></a>
            <div class="media-title">
              Lorem ipsum
            </div>
            <div class="media-text">
              dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...
            </div>
          </div>
        </div>
        <footer>
          <!-- <a href="gallery.html" class="btn btn-danger btn-lg" style="opacity: 0.7;">Full Gallery</a> -->
          <div class="container sud" >
            <button class="btn-pill">
              <span>Get More Photos</span>
            </button>
          </div>
        </footer>
      </div>
  </section>


  <!--/#portfolio-item-->


  <footer>
    <div class="footer">
      <div class="container">
        <div class="social-icon">
          <div class="col-md-4">
            <ul class="social-network">
              <li><a href="#" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="linkedin tool-tip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#" class="ytube tool-tip" title="You Tube"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-4 col-md-offset-4">
          <div class="copyright">
            &copy; Company Theme. All Rights Reserved.
            <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Company
              -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a></div>
          </div>
        </div>
      </div>
      <div class="pull-right">
        <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
      </div>
    </div>
  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>


  <!-- <script src="assets/js/jquery.poptrox.min.js"></script> -->
  <script src="assets/js/jquery.scrolly.min.js"></script>
  <script src="assets/js/skel.min.js"></script>
  <script src="assets/js/util.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
    $(".media").click(function(evt){
      evt.preventDefault();
      window.open("tour-package.php");
    });
  </script>
</body>

</html>
