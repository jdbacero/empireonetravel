<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>EmpireOne Travel - Travel Agency in Toronto | Cheap Flights to Manila</title>
  <link rel="shortcut icon" href="images1/logo.png"/>

  <!-- icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/styles.css">
  <link href="https://fonts.googleapis.com/css?family=Permanent+Marker&display=swap" rel="stylesheet">

  <!-- jQuery Modal -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

  <link href="css/prettyPhoto.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet" />

  <style media="screen">
    .fa, .fas {
      box-shadow:none;
    }
    .fa:hover, .fas:hover {
  box-shadow: 0px 0px 9px 4px white;
  animation: glowShadow 1.5s linear infinite alternate;

    }
    #partner {
      background: url(images/gut.gif) 50% 50% no-repeat;
      background-size: cover;
      background-attachment: fixed;
    }
  </style>
  <!-- =======================================================
    Theme Name: Company
    Theme URL: https://bootstrapmade.com/company-free-html-bootstrap-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
<?php include('header.php'); ?>
  <!-- <section id="main-slider" class="no-margin"> -->
    <div class="slider-container">
      <div class="slider-control left inactive"></div>
      <div class="slider-control right"></div>
      <ul class="slider-pagi"></ul>
      <div class="slider">
        <div class="slide slide-0 active">
          <div class="slide__bg"></div>
          <div class="slide__content">
            <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
              <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
            </svg>
            <div class="slide__text">
              <h2 class="slide__text-heading">WHO <b style="color:green;">WE ARE</b></h2>
              <p class="slide__text-desc">Here at EmpireOne Travel, Inc. we are proud to offer our customers the absolute best in travel services.
                We are certified by the Travel Industry Council of Ontario Tico <b style="color: green;">#50021990</b>, and nothing gives us more of a thrill than being able to get a great deal for our customers on the route of their dreams. Specializing in routes to and from the Philippines, we want our customers to have the best experience from picking up the tickets at the airport, to landing on the sunny beaches and enjoying a once in a lifetime vacation.</p>
              <!-- <a class="slide__text-link">GET INFO</a> -->
            </div>
          </div>
        </div>
        <div class="slide slide-1 ">
          <div class="slide__bg"></div>
          <div class="slide__content">
            <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
              <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
            </svg>
            <div class="slide__text">
              <h2 class="slide__text-heading">WHAT <b style="color: green;">WE DO </b></h2>
              <p class="slide__text-desc">
                According to U.S. News and World Report, over <b style="color: green;">30% of travelers surveyed</b> said it took over 5 hours online just to book some tickets. Five hours of stress, five hours of frustration, five hours that feel like the complete opposite of what a vacation should feel like. Who needs more stress in their life?
                <br><br>
                While we specialize in roundtrips from <b style="color: green;">Toronto to Manila,</b> we can help you with travel to anywhere in the world. Don’t be afraid to tell us about that once in a lifetime trip you’ve been dreaming of. You might be amazed how <b style="color: green;">affordable that dream could be.</b>
              </p>
              <!-- <a class="slide__text-link">Project link</a> -->
            </div>
          </div>
        </div>
        <div class="slide slide-2">
          <div class="slide__bg"></div>
          <div class="slide__content">
            <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
              <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
            </svg>
            <div class="slide__text">
              <h2 class="slide__text-heading">WHY <b style="color: green;">CHOOSE US</b></h2>
              <p class="slide__text-desc">
                <b style="color: green;">EmpireOne Travel, Inc. is on your side by offering the best in customer service.</b> Some people love glitz and glamour. Others want to immerse themselves in the natural culture of an area. You will find that we listen to your specific needs to make sure you get the vacation that truly suits your specific desires.
                <br>
                <br>
                If you’re really a tough cookie and still aren’t quite convinced, then take a look at why you want <b style="color: green;">EmpireOne Travel, Inc.</b> representing you:


              </p>
              <!-- <a class="slide__text-link">Project link</a> -->
            </div>
          </div>
        </div>
        <div class="slide slide-3">
          <div class="slide__bg"></div>
          <div class="slide__content">
            <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
              <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
            </svg>
            <div class="slide__text">
              <h2 class="slide__text-heading">IMA<b style="color: green;">GINE</b></h2>
              <p class="slide__text-desc">
                The golden sunset is shooting orange waves across the <b style="color: green">Pacific Ocean</b> waters as you listen to the smooth,
                steady lapping of the tide against the beach’s warm inviting sand.
                Enjoy amazing nightlife where your dollars go far, and delectable local dishes at the best restaurants.
                Let your stress just slip gently away while you enjoy <b style="color: green;">sunny beaches, fantastic food, and an
                open and wonderful hospitality</b> that is second to none.
                <br>
                <br>

              </p>
              <!-- <a class="slide__text-link">Project link</a> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="feature" style="background: white 50% 50% no-repeat;
  background-size: cover;
  background-attachment: fixed;">
    <div class="container">
      <div class="text-center">
        <div class="col-md-3">
          <div class="hi-icon-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <i class="fas fa-bus iconhover"></i>
            <h2>Bus Tours</h2>
            <p>Quisque eu ante at tortor imperdiet gravida nec sed turpis phasellus.</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <i class="fa fa-plane iconhover"></i>
            <h2>Flights</h2>
            <p>Quisque eu ante at tortor imperdiet gravida nec sed turpis phasellus.</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms">
            <i class="fas fa-layer-group iconhover"></i>
            <h2>Tour Packages</h2>
            <p>Quisque eu ante at tortor imperdiet gravida nec sed turpis phasellus.</p>
          </div>
        </div>
        <div class="col-md-3">
          <div class="hi-icon-wrap wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms">
            <i class="fas fa-box-open iconhover"></i>
            <h2>All Inclusive</h2>
            <p>Quisque eu ante at tortor imperdiet gravida nec sed turpis phasellus.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="about">
    <div class="container">
      <div class="col-md-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
        <h2>EMPIREONE <b style="color: green">TRAVEL TEAM</b> </h2>
        <img src="images/slider/slide.jpg" class="img-responsive" />
        <p><b style="color: darkblue;">WHY CHOOSE US </b> <br><br>
          EmpireOne Travel, Inc. is on your side by offering the best in customer service. Some people love glitz and glamour. Others want to immerse themselves in the natural culture of an area. You will find that we listen to your specific needs to make sure you get the vacation that truly suits your specific desires.

          You’re not working with amateurs here. Every single one of our agents has at least five years of experience in the travel industry. We’re TICO registered travel agents who know how to speak the language, search out the best deals, and make sure every one of our customers is taken care of.
        </p>
      </div>

      <!-- <div class="col-md-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <h2>EMPIREONE HISTORY</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero,
          pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
            libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
          </p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
            libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque </p>
      </div> -->
    </div>
  </div>

  <div class="lates">
    <div class="container con">
      <div class="text-center">
        <h2>Lates News</h2>
      </div>
      <div class="col-md-6 wow fadeInDown ow" data-wow-duration="1000ms" data-wow-delay="300ms">
        <img src="images/latest-1.jpg" class="img-responsive" />
        <div class="overlay overlay-left">
            <div class="text"><button class="btn-lg btn-1">GET INFO</button></div>
          </div>
        </div>
        <!-- <h3>Template built with Twitter Bootstrap</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero,
          pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
        </p> -->


      <div class="col-md-6 wow fadeInDown ow" data-wow-duration="1000ms" data-wow-delay="600ms">
        <img src="images/latest-2.jpg" style="height: 360px; width: 100%;" class="img-responsive" />
        <div class="overlay overlay-left">
            <div class="text">
              <a href="#">
                <button class="btn-lg btn-1">GET INFO</button>
              </a>
            </div>
          </div>
        </div>
        <!-- <h3>Template built with Twitter Bootstrap</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero, pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque libero,
          pulvinar tincidunt leo consectetur eget. Curabitur lacinia pellentesque
        </p> -->
      </div>
    </div>
  </div>

  <?php include('modal/bus-modal.php') ?>

  <section id="partner">
    <div class="container">
      <div class="center wow fadeInDown">
        <h2>Our Partners</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
      </div>

      <div class="partners">
        <ul>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="images/partners/partner1.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="images/partners/partner2.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="images/partners/partner3.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="images/partners/partner4.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="images/partners/partner5.png"></a></li>
        </ul>
      </div>
    </div>
    <!--/.container-->
  </section>
  <!--/#partner-->

  <section id="conatcat-info">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="media contact-info wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="pull-left">
              <i class="fa fa-phone"></i>
            </div>
            <div class="media-body">
              <h2>Have a question or need a custom quote?</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation +0123 456 70 80</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/.container-->
  </section>
  <!--/#conatcat-info-->

  <footer>
    <div class="footer">
      <div class="container">
        <div class="social-icon">
          <div class="col-md-4">
            <ul class="social-network">
              <li><a href="#" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="#" class="linkedin tool-tip" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
              <li><a href="#" class="ytube tool-tip" title="You Tube"><i class="fa fa-youtube-play"></i></a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-4 col-md-offset-4">
          <div class="copyright">
            &copy; Company Theme. All Rights Reserved.
            <div class="credits">
              <!--
                All the links in the footer should remain intact.
                You can delete the links only if you purchased the pro version.
                Licensing information: https://bootstrapmade.com/license/
                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Company
              -->
              Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a></div>
          </div>
        </div>
      </div>

      <div class="pull-right">
        <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
      </div>
    </div>
  </footer>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


  <script src="js/jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/jquery.isotope.min.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/functions.js"></script>
  <script type="text/javascript" src="js/index.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

</body>

</html>
